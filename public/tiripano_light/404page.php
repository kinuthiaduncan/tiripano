<?php 
	include("includes/header.php");
	include("includes/top_nav.php");
?>

<style type="text/css">
	nav a {
		color: #fff !important;
	}
</style>

<div class="container_wrapper text-center">
	<div class="outer_container">
		<div class="inner_content_container px-4">
			<h3 class="display-2 text-center text-white">404</h3>
			<p class="display-4 text-white text-center">Page Not Found</p>
			<p class="text-white text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
			<a href="#" class="btn btn-danger d-inline-block text-center mx-auto">Go Home</a>
		</div>
	</div>
</div>