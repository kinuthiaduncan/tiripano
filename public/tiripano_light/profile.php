<?php 
	include("includes/header.php");
	include("includes/top_nav.php");
?>

<style type="text/css">
	.navbar {
		background: green;

	}

	nav a {
		color: #fff !important;
	}

	.container .bg-light {
		background: #cccccc47 !important;
	}

	.custom_margin {
		margin:65px auto 15px auto !important;
	}

	.user_img img {
		object-fit: contain;

	}

	@media(max-width: 768px) {
		.user_img img {
			height: 100% !important;
		}

		.custom_margin > .row {
			width: 100%;
		}

		.custom_margin > .row:nth-child(1) {
			flex-shrink: 0 !important;

		}
	}
</style>

<div class="container custom_margin">
 <div class=" py-2 row px-4">
 	<div class="bg-light mt-3 mb-3 col-md-3 p-3">
 		<div class="user_img">
 			<img src="assets/images/boy.jpeg" style="width: 100%; height: 300px;">
 		</div>
 		<div class="work_part d-flex align-items-center">
 			<h5 class="text-secondary">Work</h5>
 			<hr>
 		</div>
 		<div class="rand_work">
 			<div class="top_bar">
 				<h4 class="text-secondary">Random Work 1</h4>
 				<a href="#" class="rounded  text-success light_bg_btn py-1 px-2">Primary</a>
 			</div>
 			<h6 class="text-secondary">Work Detail's</h6>
 			<h6 class="text-secondary">Detail's Location</h6>
 		</div>

 		<div class="rand_work">
 			<div class="top_bar">
 				<h4 class="text-secondary">Random Work 2</h4>
 				<a href="#" class="rounded  text-success light_bg_btn py-1 px-2">Primary</a>
 			</div>
 			<h6 class="text-secondary">Work Detail's</h6>
 			<h6 class="text-secondary">Detail's Location</h6>
 		</div>

 		<div class="work_part">
 			<div class="d-flex align-items-center">
 				<h5 class="text-secondary">Skills</h5>
 				<hr>
 			</div>
 			<div class="skill_part">
 				<h6 class="text-uppercase text-secondary">SKILL I</h6>
 				<h6 class="text-uppercase text-secondary">SKILL II</h6>
 				<h6 class="text-uppercase text-secondary">SKILL III</h6>
 				<h6 class="text-uppercase text-secondary">SKILL IV</h6>
 				<h6 class="text-uppercase text-secondary">SKILL V</h6>
 				<h6 class="text-uppercase text-secondary">SKILL VI</h6>
 				<h6 class="text-uppercase text-secondary">SKILL VII</h6>

 			</div>
 			<hr>
 		</div>

 	</div>
 	<div class="col-md-9  mt-3 mb-3 px-0">
 		<div class="container px-0">
 			<div class="bg-light  p-3 right_profile_content">
 			<div class="d-flex align-items-center justify-content-between">
 				<h4 class="text-secondary">SP Name</h4>
 				<div class="d-flex align-items-center">
 					<i class="fas fa-map-marker-alt mr-2"></i>
 					<span class="text-secondary">Location</span>
 				</div>
 			</div>
 			<a href="#" class="text-success">Product Designer</a>
 			<div class="mt-4 mb-4 d-flex align-items-center">
 				<h6 class="mb-0 text-dark font-weight-bold mr-1">8, 6</h6>
 				<i class="fas fa-star light_star"></i>
 				<i class="fas fa-star light_star"></i>
 				<i class="fas fa-star light_star"></i>
 				<i class="fas fa-star light_star"></i>
 				<i class="fas fa-star semi_light_star"></i>
 			</div>
 			<div class="mt-4 mb-4 d-flex align-items-center">
 					<i class="fas fa-comment-alt mr-2"></i>
 					<a class="mb-0 text-dark  mr-2">Send Message</a>
 					<a class="mb-0 text-dark ">Report User</a>
 			</div>
 			
 			<div class="mt-4 mb-4 d-flex align-items-center">
 				<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item d-flex align-items-center">
  	<i class="far fa-eye text-dark mr-2"></i>
    <a class="text-dark nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">
    	 My Services</a>
  </li>
  <li class="nav-item d-flex align-items-center">
  	<i class="fas fa-user ml-4 mr-2"></i>
    <a class="nav-link text-dark" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"> About</a>
  </li>
 
</ul>
		


 			</div>

				<div class="tab-content" id="pills-tabContent ">
					<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
									<div class="services">
				<div class="custom_grid">
					<div class="one bg-white">
						<div class="image">
							<img src="assets/images/general_image1.jpg" class="w-100 h-100">
						</div>
						<div class="review_section py-2">
							<h5 class="text-center text-dark font-weight-bold mt-2 mb-2">Service Name</h5>
							<a href="#" class=" d-inline-block  text-center rounded  py-1 px-3">Book / $10</a>
						</div>
					</div>

					<div class="two bg-white">
						<div class="image">
							<img src="assets/images/general_image1.jpg" class="w-100 h-100">
						</div>
						<div class="review_section py-2">
							<h5 class="text-center text-dark font-weight-bold mt-2 mb-2">Service Name</h5>
							<a href="#" class=" d-inline-block  text-center rounded  py-1 px-3">Book / $10</a>
						</div>
					</div>

					<div class="third bg-white">
						<div class="image">
							<img src="assets/images/general_image2.jpg" class="w-100 h-100">
						</div>
						<div class="review_section py-2">
							<h5 class="text-center text-dark font-weight-bold mt-2 mb-2">Service Name</h5>
							<a href="#" class=" d-inline-block  text-center rounded py-1 px-3">Book / $10</a>
						</div>
					</div>


				</div>


				</div>
					</div>
					<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
						<h5 class="h5 text-secondary text-uppercase font-weight-bold mb-4">CONTACT INFORMATION</h5>
						<div class="user_personal_details bg-white p-2 mb-4">
							<div class="mt-2 mb-2">
								<h5 class="small_font text-secondary mr-2">Phone:</h5>
								<h5 class="small_font">(000)-123-123</h5>
							</div>
							<div class="mt-2 mb-2">
								<h5 class="small_font text-secondary mr-2">Address:</h5>
								<h5 class="small_font">Address</h5>
							</div>
							<div class="mt-2 mb-2">
								<h5 class="small_font text-secondary mr-2">Email:</h5>
								<h5 class="small_font">Email</h5>
							</div>
							<div class="mt-2 mb-2">
								<h5 class="small_font text-secondary mr-2">Site:</h5>
								<h5 class="small_font">Site</h5>
							</div>
							<div class="mt-2 mb-2">
								<h5 class="text-secondary mr-2 font-weight-bold">Basic Information:</h5>
							</div>
							<div class="mt-2 mb-2">
								<h5 class="small_font text-secondary mr-2">Birthday:</h5>
								<h5 class="small_font">Birthday</h5>
							</div>
							<div class="mt-2 mb-2">
								<h5 class="small_font text-secondary mr-2">Gender:</h5>
								<h5 class="small_font">Gender</h5>
							</div>

							<div class="mt-2 mb-2">
								<h5 class="small_font text-secondary mr-2">Password Update:</h5>
								<h5 class="small_font">Password Update</h5>
							</div>


						</div>
					</div>
				</div>

					<div class="mt-2 comments bg-white p-2">
						<h4 class="text-secondary text-left font-weight-bold">Reviews</h4>
						<hr>
						<div class="d-flex flex-column p-2">
							<div>
								<img class="mr-2" src="assets/images/general_image.png" style="width: 25px; height: 25px; border-radius: 50%;">
								<span class="mr-1 text-secondary">baranga24</span>		
							</div>
							<div class="d-flex">
								<i class=" fas fa-star text-warning ml-4 pl-2"></i>
							<i class=" fas fa-star text-warning  pl-2"></i>
							<i class=" fas fa-star text-warning  pl-2"></i>
							<i class=" fas fa-star text-warning  pl-2"></i>
							<i class=" fas fa-star text-warning  pl-2"></i>
							</div>
						</div>
						<div class="comment p-1">
							<p class="text-dark mb-0 pl-4 mt-2 ml-2">(Some User Name) listened to all of our requirements and created a beautiful site for us! He provided outstanding customer service along the entire process with his fantastic attitude and (seemingly) infinite patience with our multitude of questions. We are already going to engage him for additional work as we know he is a a talented, high-quality resource!!</p>
							<div class="duration">
								<p class="small mt-4 mb-0">about 19 hours ago</p>
							</div>
						</div>
						<hr>

						<div class="d-flex align-items-center p-2">
							<i class="fas fa-plus text-success mr-2"></i>
							<a href="#" class="text-success mb-0">See More</a>
						</div>


 		
					</div>
			</div>



 		</div>
 		</div>
 	</div>
 </div>	
</div>



<?php 
	include("includes/footer.php");
?>


