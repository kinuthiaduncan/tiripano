<?php 
	include("includes/header.php");
	include("includes/top_nav.php");
?>

<style type="text/css">
	nav a{
   	color: #fff !important;
} 
</style>

<div class="container">
	<div class="content_wrapper">
		<div class="inner_content">
			<div class="align-self-center left_content">
				<!-- TABS GOES IN HERE -->
			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				<li class="nav-item">
				<a class="nav-link active text-dark" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Sign up</a>
				</li>
				<li class="nav-item">
				<a class="nav-link text-dark" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Sign In</a>
				</li>
			</ul>

			<div class="tab-content" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
					<div class="form_container">
					<form>
						<label class="text-dark small">First Name</label>
						<input type="text" class="input_element" autocomplete="off" name="name">
						<label class="text-dark small">Last Name</label>
						<input type="text" class="input_element" autocomplete="off" name="lname">
						<label class="text-dark small">Email</label>
						<input type="email" class="input_element" autocomplete="off" name="email">
						<label class="text-dark small">Password</label>
						<input type="password" class="input_element" autocomplete="off" name="password">
						<div class="mb-4 d-flex align-items-center">
							<input class="mr-2" type="checkbox" name="checkbox">
							<p class="text-dark mb-0  small">Terms Of Service</p>
						</div>
						<input type="submit" class="mt-4 mb-4 p-2 input_element submit_btn rounded" name="submit" value="Sign In">
						<a href="#" class="text-dark small">Already a member?  Sign In</a>
					</form>
				</div>
				</div>
				<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
					<div class="form_container">
					<form>
						<label class="text-dark small">Email</label>
						<input type="email" class="input_element" autocomplete="off" name="email">
						<label class="text-dark small">Password</label>
						<input type="password" class="input_element" autocomplete="off" name="password">
						<input type="submit" class="p-2 mb-2 input_element submit_btn rounded" name="submit" value="Sign Up">
						<a href="#" class="text-dark small">Not a member?  Sign Up</a>
					</form>
				</div>
				</div>
			</div>
				<!-- TABS CLOSES IN HERE -->
			</div>
			<div class="right_content">

			</div>
		</div>
	</div>
</div>