<?php 
	include("includes/header.php");
	include("includes/top_nav.php");
?>


<style type="text/css">

	body {
		background: #cccccc5e;
	}

	nav {
		background: var(--dark-green);

	}

	nav a {
		color: #fff !important;
	}

</style>

<div class="container booking_webpage p-4">
	<div class="row">
		
		<div class="col-md-6 top_col_snd">
			<h6 class="text-secondary text-left font-weight-light">Time</h6>
			<div class="d-flex align-items-center">
				 <input id="timepicker1" type="text" name="timepicker1">
			</div>
		</div>
		<div class="col-md-6 top_col_first">
			<h6 class="text-secondary text-left font-weight-light">Date</h6>
			<input type="date" name="date">
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 first_col">
			<h5 class="text-secondary text-center font-weight-bold mt-2 mb-2">Choose Your Location</h5>
			<form>
				<label>City</label>
				<input type="text" name="cityName">
				<label>Zip Code</label>
				<input type="number" name="zipCode">
				<label>State/Provience</label>
				<input type="text" name="state/Provience">
				<label>Country</label>
				<input type="text" name="country">
			</form>
		</div>
		<div class="col-md-6 snd_col">
			<h5 class="font-weight-bold text-center text-secondary mt-2 mb-2">Payment Options</h5>
			<ul class="ml-auto">
				<li>
					<input type="radio" name="stripe"
					>
					<span class="text-secondary">Stripe</span>
				</li>
				<li>
					<input type="radio" name="stripe"
					>
					<span class="text-secondary">Credit/Debit Card</span>
				</li>
				<li>
					<input type="radio" name="stripe"
					>
					<span class="text-secondary">Paypal</span>
				</li>
				<a href="#" class="text-center text-white  py-2 rounded  custom_green_btn">Next</a>
			</ul>
			
		</div>
	</div>
</div>

<script type="text/javascript">
	 $( function() {
        $('#timepicker1').timepicki();
      } );
</script>

<?php 
	include("includes/footer.php");
?>
