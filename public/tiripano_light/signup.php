<?php 
	include("includes/header.php");
	include("includes/top_nav.php");
?>

<div class="container mt-4 mb-4">
	<div class="content_wrapper">
		<div class="inner_content">
			<div class="left_content">
				<!-- TABS GOES IN HERE -->

				<!-- TABS CLOSES IN HERE -->
				<div class="form_container">
					<form>
						<label class="text-dark small">First Name</label>
						<input type="text" class="input_element" name="name">
						<label class="text-dark small">Last Name</label>
						<input type="text" class="input_element" name="lname">
						<label class="text-dark small">Email</label>
						<input type="email" class="input_element" name="email">
						<label class="text-dark small">Password</label>
						<input type="password" class="input_element" name="password">
						<div class="">
							<input type="checkbox" name="checkbox">
							<p class="text-dark mb-0">Terms Of Service</p>
						</div>
						<input type="submit" class="input_element input_btn" name="submit">
					</form>
				</div>
			</div>
			<div class="right_content">

			</div>
		</div>
	</div>
</div>