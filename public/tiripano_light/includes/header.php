<!DOCTYPE html>
<html>

<head>
    <title>DarDingli | DarDingli</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A Real Estate Website" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link href="assets/css/timepicki.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,700,700i" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
   
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" crossorigin="anonymous">
    
    <script src="assets/js/jquery_new.js"></script>
 
 
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.js"></script>
       <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/timepicker.js"></script>

</head>
