	<footer>
	<div class="mt-2 mb-2">
		<p class="text-white font-weight-bold">Placeholder LOGO</p>
		<p class="text-white">copyright &copy; 2019, All Rights Reserved</p>
	</div>
	<div class="mt-2 mb-2">
		<h3 class="text-white font-weight-bold heading_tertiary">Quick Links</h3>
		<a class="text-white" href="#">Superhelper</a>
		<a class="text-white" href="#">Jobs</a>
		<a class="text-white" href="#">Careers</a>
		<a class="text-white" href="#">About Us</a>
	</div>
	<div class="mt-2 mb-2">
		<h3 class="text-white font-weight-bold heading_tertiary">About Us</h3>
		<a class="text-white" href="#">Contact Us</a>
		<a class="text-white" href="#">Privacy Policy</a>
		<a class="text-white" href="#">Terms and Conditions</a>
		<a class="text-white" href="#">FAQ</a>
		<a href="#" class="text-white">Impressum</a>
	</div>
	<div class="mt-2 mb-2">
		<h3 class="text-white font-weight-bold heading_tertiary">Follow Us</h3>
		<a class="text-white" href="#">Facebook</a>
		<a class="text-white" href="#">Youtube</a>
		<a class="text-white" href="#">Instagram</a>
		<a class="text-white" href="#">Twitter</a>
		<a href="#" class="text-white">Blog</a>
	</div>
</footer>