<nav id="navbar-example2" class="navbar navbar-expand-lg custom_nav ">
    <div class="container">
        <a class="navbar-brand" href="index.php"><img src="assets/images/logo 1.png" style="width: 100px;"></a>
        <button class="navbar-toggler px-0 py-2" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon w-100 px-2 py-1" style="transform: rotate(270deg); color: #ccc; background: green;">|||</span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active active_item">
                    <a class="nav-link scroll custom_link_item " href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll custom_link_item" href="#section_five">Why Tripano</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll custom_link_item" href="#section-six">How It Works</a>
                </li>
            </ul>
            {{--@if (Route::has('login'))--}}
            {{--<div class="top-right links">--}}
            {{--@if (Auth::check())--}}
            {{--<a href="{{ url('/home') }}">Home</a>--}}
            {{--@else--}}
            {{--<a href="{{ url('/login') }}">Login</a>--}}
            {{--<a href="{{ url('/register') }}">Register</a>--}}
            {{--@endif--}}
            {{--</div>--}}
            {{--@endif--}}
            @if (Auth::check() == false)
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link custom_link_item" href="{{ url('/login') }}">LogIn / Sign Up <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            @endif
        </div>
    </div>
</nav>