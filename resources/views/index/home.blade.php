@extends('layouts.index')

@section('content')
    <section id="section-1" class="w-100 section_1">
        @include('partials.header')
        <div class="wrapper" >
            <div class="headings">
                <h2 class="display-4 custom_heading_primary">AnyWhere.</h2>
                <h2 class="display-4 custom_heading_primary">Anyprice.</h2>
                <h2 class="display-4 custom_heading_primary">Anytime.</h2>
            </div>

            <div class="button">
                <a href="#" class="custom_semi_transparent_btn">Get Started</a>
            </div>
        </div>
        <div class="w-100" class="video">
            <video id="vid"  playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                <source src="assets/videos/vid.mp4" type="video/mp4">
            </video>
        </div>
    </section>


    <section class="section_2">
        <h1 class="text-center  custom_heading_primary">Our Services</h1>
        <div class="container">
            <div class="row  mt-2 mx-auto mb-2 justify-content-around">
                <div class="col-md-4 ">
                    <div class="rounded_borders">
                        <div class="sr pt-3  mt-4 mb-2">
                            <h4 class="text-center">Child Care</h4>
                            <p class="p-4 text-left  mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt </p>
                            <a href="#" class="btn btn-block btn_custom_green text-white py-2 px-1">Hire A Professional</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="rounded_borders">
                        <div class="sr pt-3  mt-4 mb-2">
                            <h4 class="text-center heading_tertiary_sub">Handyman</h4>
                            <p class="p-4 text-left  mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt </p>
                            <a href="#" class="btn btn-block btn_custom_green text-white py-2 px-1">Hire A Professional</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="rounded_borders">
                        <div class="sr pt-3  mt-4 mb-2">
                            <h4 class="text-center heading_tertiary_sub">Cleaner</h4>
                            <p class="p-4 text-left  mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt </p>
                            <a href="#" class="btn btn-block btn_custom_green text-white py-2 px-1">Hire A Professional</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-2 mx-auto mb-2 justify-content-around">
                <div class="col-md-4 ">
                    <div class="rounded_borders">
                        <div class="sr pt-3  mt-4 mb-2">
                            <h4 class="text-center heading_tertiary_sub">Senior Care</h4>
                            <p class="p-4 text-left  mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt </p>
                            <a href="#" class="btn btn-block btn_custom_green text-white py-2 px-1">Hire A Professional</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="rounded_borders">
                        <div class="sr pt-3  mt-4 mb-2">
                            <h4 class="text-center heading_tertiary_sub">Tutor</h4>
                            <p class="p-4 text-left  mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt </p>
                            <a href="#" class="btn btn-block btn_custom_green text-white py-2 px-1">Hire A Professional</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="rounded_borders">
                        <div class="sr pt-3  mt-4 mb-2">
                            <h4 class="text-center heading_tertiary_sub">Home Service</h4>
                            <p class="p-4 text-left  mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt </p>
                            <a href="#" class="btn btn-block btn_custom_green text-white py-2 px-1">Hire A Professional</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>


    <section id="section-3" class="section_3">
        <h1 class="text-center custom_heading_primary">Superhelper Review</h1>
        <div class="custom_container custom_padding">
            <div class="row px-4  justify-content-around">
                <div class="col-md-4">
                    <div class="row bg_white mt-2 mb-2 py-4 justify-content-around mt-2 mb-2">
                        <div class="col-lg-5 col-md-12 col-sm-12">
                            <img src="assets/images/user.jpg" style="width: 100%; height: auto">
                        </div>
                        <div class="col-md-7 col-xs-12">
                            <h5 class="text-left font-weight-bold mb-0 heading_tertiary_sub">Client's Name</h5>
                            <p class="text-white text-left mb-0">Service</p>
                            <div class="icons mt-2">
							<span class="bg_yellow">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow ">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow text-white">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                            </div>
                            <p class="mt-1 mb-0">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="row bg_white mt-2 mb-2 py-4">
                        <div class="col-lg-5 col-md-12">
                            <img src="assets/images/user.jpg" style="width: 100%; height: auto">
                        </div>
                        <div class="col-md-7">
                            <h5 class="text-left font-weight-bold mb-0 heading_tertiary_sub">Client's Name</h5>
                            <p class="text-white text-left mb-0">Service</p>
                            <div class="icons mt-2">
							<span class="bg_yellow">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow ">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow text-white">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                            </div>
                            <p class="mt-1 mb-0">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="row bg_white mt-2 mb-2 py-4">
                        <div class="col-lg-5 col-md-12">
                            <img src="assets/images/user.jpg" style="width: 100%; height: auto">
                        </div>
                        <div class="col-md-7">
                            <h5 class="text-left font-weight-bold mb-0 heading_tertiary_sub">Client's Name</h5>
                            <p class="text-white text-left mb-0 ">Service</p>
                            <div class="icons mt-2">
							<span class="bg_yellow">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow ">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow text-white">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                            </div>
                            <p class="mt-1 mb-0">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4 mt-4 px-4  justify-content-around">
                <div class="col-md-4">
                    <div class="row bg_white mt-2 mb-2 py-4">
                        <div class="col-lg-5 col-md-12">
                            <img src="assets/images/user.jpg" style="width: 100%; height: auto">
                        </div>
                        <div class="col-md-7">
                            <h5 class="text-left font-weight-bold mb-0 heading_tertiary_sub">Client's Name</h5>
                            <p class="text-white text-left mb-0">Service</p>
                            <div class="icons mt-2">
							<span class="bg_yellow">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow ">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow text-white">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                            </div>
                            <p class="mt-1 mb-0">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="row bg_white mt-2 mb-2 py-4">
                        <div class="col-lg-5 col-md-12">
                            <img src="assets/images/user.jpg" style="width: 100%; height: auto">
                        </div>
                        <div class="col-md-7">
                            <h5 class="text-left font-weight-bold mb-0 heading_tertiary_sub" >Client's Name</h5>
                            <p class="text-white text-left mb-0">Service</p>
                            <div class="icons mt-2">
							<span class="bg_yellow">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow ">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow text-white">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                            </div>
                            <p class="mt-1 mb-0">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="row bg_white mt-2 mb-2 py-4">
                        <div class="col-lg-5 col-md-12">
                            <img src="assets/images/user.jpg" style="width: 100%; height: auto">
                        </div>
                        <div class="col-md-7">
                            <h5 class="text-left font-weight-bold mb-0 heading_tertiary_sub">Client's Name</h5>
                            <p class="text-white text-left mb-0">Service</p>
                            <div class="icons mt-2">
							<span class="bg_yellow">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow ">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_yellow text-white">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                                <span class="bg_grey">
								<i class="fas fa-star  text-white"></i>
							</span>
                            </div>
                            <p class="mt-1 mb-0">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
    <section  class="section_4">
        <div class="container">
            <h1 class="text-center custom_heading_primary">Latest Jobs</h1>
            <!-- <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
            <div class="row">
                    <div class="carousel-item active">
                       <div class="row">
                    <div class="col-xs-1 col-md-4 custom_col flex-grow-2">
                    <div class="box text-center">
                    <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                    <h3>Job Name</h3>
                    <h4>$ 120</h4>
                    <p class="text-white">Daily</p>
                    <a href="#" class="custom_transparent_btn">Apply</a>
                    </div>
                    </div>
                    <div class="col-xs-1 col-md-4 custom_col">
                    <div class="box text-center">
                    <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                    <h3>Job Name</h3>
                    <h4>$ 120</h4>
                    <p class="text-white">Daily</p>
                    <a href="#" class="custom_transparent_btn">Apply</a>
                    </div>
                    </div>
                    <div class="col-xs-1 col-md-4 custom_col">
                    <div class="box text-center">
                    <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                    <h3>Job Name</h3>
                    <h4>$ 120</h4>
                    <p class="text-white">Daily</p>
                    <a href="#" class="custom_transparent_btn">Apply</a>
                    </div>
                    </div>
              </div>
                </div>
            </div>
            </div>
            </div> -->

            <div class="owl-carousel owl-theme">
                <!-- <div class="col-xs-1 col-md-4 custom_col flex-grow-2">
                <div class="box text-center">
                <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                <h3>Job Name</h3>
                <h4>$ 120</h4>
                <p class="text-white">Daily</p>
                <a href="#" class="custom_transparent_btn">Apply</a>
                </div>
                </div>
                <div class="col-xs-1 col-md-4 custom_col">
                <div class="box text-center">
                <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                <h3>Job Name</h3>
                <h4>$ 120</h4>
                <p class="text-white">Daily</p>
                <a href="#" class="custom_transparent_btn">Apply</a>
                </div>
                </div>
                <div class="col-xs-1 col-md-4 custom_col">
                <div class="box text-center">
                <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                <h3>Job Name</h3>
                <h4>$ 120</h4>
                <p class="text-white">Daily</p>
                <a href="#" class="custom_transparent_btn">Apply</a>
                </div>
                </div> -->

                <div class="item-1">
                    <div class="box text-center">
                        <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                        <h3 class="heading_tertiary">Job Name</h3>
                        <h4>$ 120</h4>
                        <p class="text-white">Daily</p>
                        <a href="#" class="custom_transparent_btn">Apply</a>
                    </div>
                </div>
                <div class="item-1">
                    <div class="box text-center">
                        <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                        <h3 class="heading_tertiary">Job Name</h3>
                        <h4>$ 120</h4>
                        <p class="text-white">Daily</p>
                        <a href="#" class="custom_transparent_btn">Apply</a>
                    </div>
                </div>
                <div class="item-1">
                    <div class="box text-center">
                        <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                        <h3 class="heading_tertiary">Job Name</h3>
                        <h4>$ 120</h4>
                        <p class="text-white">Daily</p>
                        <a href="#" class="custom_transparent_btn">Apply</a>
                    </div>
                </div>
                <div class="item-1">
                    <div class="box text-center">
                        <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                        <h3 class="heading_tertiary">Job Name</h3>
                        <h4>$ 120</h4>
                        <p class="text-white">Daily</p>
                        <a href="#" class="custom_transparent_btn">Apply</a>
                    </div>
                </div>
                <div class="item-1">
                    <div class="box text-center">
                        <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                        <h3 class="heading_tertiary">Job Name</h3>
                        <h4>$ 120</h4>
                        <p class="text-white">Daily</p>
                        <a href="#" class="custom_transparent_btn">Apply</a>
                    </div>
                </div>
            </div>
        </div>


    </section>


    <section id="section-5" class="section_5">
        <h2 class="text-center custom_heading_primary">Your Advantages</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box text-center">
                        <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                        <h3 class="heading_tertiary">Instant Online Jobs</h3>
                        <p class="text-dark py-4">Need a job done ASP? Our Instant booking service ensures you get it done in time</p>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box text-center">
                        <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                        <h3 class="heading_tertiary">Secure Payments</h3>
                        <p class="text-dark py-4">After a job is booked, the agreed price is first paid into the escrow, and is paid off to the service provider upon the completion of the job.</p>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box text-center">
                        <span class="d-inline-block mt-4 mb-4 bg-white"></span>
                        <h3 class="heading_tertiary">High Quality of Work</h3>
                        <p class="text-dark py-4">Our Quality control system makes sure that your work is done by trusted professionals</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <h1 class="h1 text-center custom_heading_primary">Ready to start with us?</h1>
                        </div>
                        <div class="col-md-4 d-inline-flex align-items-center text-center">
                            <a href="#" class="custom_semi_transparent_btn">Register</a>
                        </div>
                    </div>
                </div>
            </div>
    </section>


    <section id="section-6" class="section_6">
        <h1 class="text-center custom_heading_primary">How It Works</h1>
        <div class="container">
            <div class="gaint_box">
                <i class="far fa-play-circle fa-4x text-white"></i>
                <div class="w-100 h-100" class="video">
                    <video  class="w-100 h-100"  controls= "controls">
                        <source src="assets/videos/vid.mp4" type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
    </section>

    <section class="section_7">
        <div class="col-12">

        </div>
    </section>
@endsection