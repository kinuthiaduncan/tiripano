<div class="form_container">
    <form>
        <label class="text-dark small">Email</label>
        <input type="email" class="input_element" autocomplete="off" name="email">
        <label class="text-dark small">Password</label>
        <input type="password" class="input_element" autocomplete="off" name="password">
        <input type="submit" class="p-2 mb-2 input_element submit_btn rounded" name="submit" value="Sign Up">
        <a href="#" class="text-dark small">Not a member?  Sign Up</a>
    </form>
</div>