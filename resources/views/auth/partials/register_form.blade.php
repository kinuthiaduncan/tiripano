<div class="form_container">
    <form>
        <label class="text-dark small">First Name</label>
        <input type="text" class="input_element" autocomplete="off" name="name">
        <label class="text-dark small">Last Name</label>
        <input type="text" class="input_element" autocomplete="off" name="lname">
        <label class="text-dark small">Email</label>
        <input type="email" class="input_element" autocomplete="off" name="email">
        <label class="text-dark small">Password</label>
        <input type="password" class="input_element" autocomplete="off" name="password">
        <div class="mb-4 d-flex align-items-center">
            <input class="mr-2" type="checkbox" name="checkbox">
            <p class="text-dark mb-0  small">Terms Of Service</p>
        </div>
        <input type="submit" class="mt-4 mb-4 p-2 input_element submit_btn rounded" name="submit" value="Sign In">
        <a href="#" class="text-dark small">Already a member?  Sign In</a>
    </form>
</div>