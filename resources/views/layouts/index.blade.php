<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A Real Estate Website" />

        <title>Tiripano</title>

        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
        <link href="assets/css/timepicki.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,700,700i" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
              integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>

    </head>
    <body>
        <div id="app">
            {{--@include('partials.header')--}}
            <div v-cloak>
                @yield('content')
            </div>
            @include('partials.footer')
        </div>
        <script src="assets/js/popper.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.js"></script>
        <script src="/js/jqueryScripts.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/timepicker.js"></script>
        <script src="{{mix('/js/app.js')}}"></script>
    </body>
</html>
